<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Id extends CI_Controller {

	public function index()
	{
		$data['judul_singkat'] = "Sejarah Singkat";

		$this->load->view('header/index');
			$this->load->view('beranda/tentang',$data);
		$this->load->view('footer/index');
	}

	public function maknalogo(){
		$data['judul_logo'] = "Makna dan Kandungan Logo UUI";


		$this->load->view('header/index');
			$this->load->view('beranda/maknalogo',$data);
		$this->load->view('footer/index');
	}

	public function fasilitas(){
		$data['fasilitas'] = "Fasilitas Sarana dan Prasarana";

		$this->load->view('header/index');
			$this->load->view('beranda/fasilitas',$data);
		$this->load->view('footer/index');
	}

	public function organisasi(){
		$data['organisasi'] = "Struktur Organisasi UUI";

		$this->load->view('header/index');
			$this->load->view('beranda/organisasi',$data);
		$this->load->view('footer/index');
	}
	
	public function dosen(){
		$data['titledosen'] = "Direktori Dosen UUI";

		$this->load->view('header/index');
			$this->load->view('beranda/dosen',$data);
		$this->load->view('footer/index');
	}

}

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Universitas Ubudiyah Indonesia</title>
	<!-- <title> meta untuk seo
	<b:if cond='data:blog.homepageUrl == data:blog.url'>
	<data:blog.title/>
	<b:else/>
	<data:blog.pageName/>
	</b:if>
	</title>
	<meta content='Masukkan deksipsi blog anda, panjang maksimal adalah 156 karakter dan jangan lebih'/>
	<meta content='Masukkan keyword anda, contoh keyword, belajar seo blogspot' name='KEYWORDS'/> <meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8'/>
	<meta content='Indonesia' name='geo.placename'/>
	<meta name="language" content="id" />
	<meta content='Tips Belajar SEO dan bisnis online dari pakar seo' name='subject'/>
	<meta content='Belajar SEO' name='Author'/>
	<meta content='all' name='audience'/>
	<meta content='general' name='rating'/>
	<meta http-equiv='refresh' content='10'/>
	<meta name="revisit-after" content="1 days"/> -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500">
	<link rel="stylesheet" href=" <?php echo base_url('assets/fonts/icomoon/style.css'); ?> ">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css'); ?>">
	<link rel="shortcut icon" href="<?php echo base_url('assets/favicon.ico'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/fonts/flaticon/font/flaticon.css'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/aos.css'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/mycss.css'); ?>">
	<!--  -->

	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

	<div class="site-wrap">

		<div class="site-mobile-menu">
			<div class="site-mobile-menu-header">
				<div class="site-mobile-menu-close mt-3">
					<span class="icon-close2 js-menu-toggle"></span>
				</div>
			</div>
			<div class="site-mobile-menu-body"></div>
		</div> <!-- .site-mobile-menu -->

		<div class="site-navbar-wrap bg-white">
			<div class="site-navbar-top ">
				<div class="container py-2">
					<div class="row align-items-center">
						<div class="col-3 text-dark">
							<a href="https://twitter.com/uubudiyah" class="p-2 pl-0"><span class="icon-twitter "></span></a>
							<a href="https://www.facebook.com/cyber.uui/" class="p-2 pl-0"><span class="icon-facebook"></span></a>
							<a href="https://www.youtube.com/channel/UCUPd83PKPdHT2MQE8uZpqGQ" class="p-2 pl-0"><span class="icon-youtube-play"></span></a>
							<a href="https://www.instagram.com/universitasubudiyah/" class="p-2 pl-0"><span class="icon-instagram"></span></a>
						</div>
						<div class="col-4">
							<div id="google_translate_element"></div>
							<!-- <div class="d-flex ml-auto">
								<a href="mailto:info@uui.ac.id" class="d-flex align-items-center ml-auto mr-4">
									<span class="icon-envelope mr-2"></span>
									<span class="d-none d-md-inline-block">Email</span>
								</a>
								<a href="tel://06517555566" class="d-flex align-items-center">
									<span class="icon-phone mr-2"></span>
									<span class="d-none d-md-inline-block">(0651)-7555566</span>
								</a>
							</div> -->
						</div>
						<div class="col-5">
							<div class="d-flex ml-auto ">
								<a href="http://karil.uui.ac.id/" class="d-flex align-items-center ml-auto ">
									<span class="icon-building mr-2"></span>
									<span class="d-none d-md-inline-block">Karya ilmiah</span>
								</a>
								<a href="https://student.uui.ac.id" class="d-flex align-items-center ml-2">
									<span class="icon-tags mr-2"></span>
									<span class="d-none d-md-inline-block">Krs-Online</span>
								</a>
								<a href="https://jurnal.uui.ac.id" class="d-flex align-items-center ml-2">
									<span class="icon-newspaper-o mr-2"></span>
									<span class="d-none d-md-inline-block">jurnal</span>
								</a>
								<a href="https://sipenmaru.uui.ac.id" class="d-flex align-items-center ml-2">
									<span class="icon-sign-in mr-2"></span>
									<span class="d-none d-md-inline-block">Daftar</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="site-navbar-wrap bg-white nav">
			<div class="container">
				<div class="site-navbar bg-light">
					<div class="py-1">
						<div class="row align-items-center">
							<div class="labellogo">
								<a class="navbar-brand logouui" href="<?php echo base_url(); ?>">
									<img src="<?php echo base_url(); ?>assets/images/logo.png" width="80" height="80" alt="logouui">
								</a>
							</div>
							<div class="col-md-3">
								<h4 class="mb-1 ml-3 site-logo"><a href="<?php echo base_url(); ?>">Universitas<strong> Ubudiyah</strong>
										Indonesia
									</a></h4>
							</div>
							<div class="col-md-9">
								<nav class="site-navigation text-right" role="navigation">
									<div class="container">
										<div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span
												 class="icon-menu h3"></span></a>
										</div>
										<ul class="site-menu js-clone-nav d-none d-lg-block">
											<li><a href="<?php echo base_url(); ?>">Pendidikan</a></li>
											<li><a href="<?php echo base_url(); ?>">Penelitian</a></li>
											<li><a href="<?php echo base_url(); ?>">Mahasiswa</a></li>
											<li><a href="<?php echo base_url(); ?>">Pengabdian</a></li>

											<!-- <li class="has-children">
												<a href="">Fakultas</a>
												<ul class="dropdown arrow-top">
													<li class="has-children">
														<a href="https://fikom.uui.ac.id">Fakultas Komputer</a>
														<ul class="dropdown">
															<li><a href="https://ti.fikom.uui.ac.id">Prodi Teknik Informatika</a></li>
															<li><a href="https://si.fikom.uui.ac.id">Prodi Sistem Informasi</a></li>
														</ul>
													<li class="has-children">
														<a href="https://fikes.uui.ac.idl">Fakultas Kesehatan</a>
														<ul class="dropdown">
															<li><a href="https://d3keb.fikes.uui.ac.id/">Prodi Kebidanan(D3)</a></li>
															<li><a href="https://d4keb.fikes.uui.ac.id/">Prodi Bidan Pendidik(D4)</a></li>
															<li><a href="https://farmasi.fikes.uui.ac.id/">Prodi Farmasi</a></li>
															<li><a href="https://psikologi.fikes.uui.ac.id/">Prodi Psikologi</a></li>
															<li><a href="https://kesma.fikes.uui.ac.id/">Prodi Kesehatan Masyarakat</a></li>
															<li><a href="https://ilmugizi.fikes.uui.ac.id/">Prodi Gizi</a></li>
														</ul>
													</li>
													<li class="has-children">
														<a href="https://fkip.uui.ac.id">FKIP</a>
														<ul class="dropdown">
															<li><a href="https://pgsd.fkip.uui.ac.id/">Prodi PGSD</a></li>
															<li><a href="https://pik.fkip.uui.ac.id/">Prodi PIK</a></li>
														</ul>
													</li>
													<li class="has-children">
														<a href="https://ekonomi.uui.ac.id">Fakultas Ekonomi</a>
														<ul class="dropdown">
															<li><a href="https://akuntansi.ekonomi.uui.ac.id/">Prodi Akuntansi</a></li>
															<li><a href="https://manajemen.ekonomi.uui.ac.id/">Prodi Manajemen</a></li>
														</ul>
													</li>
													<li class="has-children">
														<a href="https://hukum.uui.ac.id">Fakultas Hukum</a>
														<ul class="dropdown">
															<li><a href="https://ilmuhukum.hukum.uui.ac.id/">Prodi Ilmu Hukum</a></li>
														</ul>
													</li>
												</ul>
											</li> -->
											<!-- <li class="has-children">
												<a href="#">Direktorat</a>
												<ul class="dropdown">
													<li><a href="https://daa.uui.ac.id/">Akademik & Administrasi</a></li>
													<li><a href="https://dkp.uui.ac.id/">Keuangan & Perencanaan</a></li>
													<li><a href="https://dsdms.uui.ac.id/">SDM dan Sarana</a></li>
													<li><a href="https://dka.uui.ac.id/">Kemahasiswaan & Alumni</a></li>
													<li><a href="https://dke.uui.ac.id/">Kerja sama</a></li>
													<li><a href="https://dppm.uui.ac.id/">Penelitian & Pengabdian</a></li>
													<li><a href="https://dpm.uui.ac.id/">Publikasi & Media</a></li>
													<li><a href="https://djm.uui.ac.id/">Jaminan Mutu</a></li>
													<li><a href="https://dpd.uui.ac.id/">Pangkalan Data</a></li>
													<li><a href="https://cdc.uui.ac.id/">Career & Development</a></li>
												</ul>
											</li> -->
											<!-- <li class="has-children">
												<a href="#">Cyber</a>
												<ul class="dropdown">
													<li><a href="https://evaluasi.uui.ac.id/">Sistem Evaluasi</a></li>
													<li><a href="https://simtakp.uui.ac.id/">Sistem TA/KP</a></li>
													<li><a href="https://wisuda.uui.ac.id/">Sistem Wisuda</a></li>
													<li><a href="https://sap.uui.ac.id/">Sistem SAP</a></li>
													<li><a href="https://arsip.uui.ac.id/">Sistem ARSIP</a></li>
													<li><a href="https://ucass.uui.ac.id/">UCASS</a></li>
													<li><a href="https://tracerstudy.uui.ac.id/">Sistem Tracer Study</a></li>
													<li><a href="https://simpeg.uui.ac.id/">Sistem Kepegawai</a></li>
													<li><a href="https://elearning.uui.ac.id/">Elearning</a></li>
													<li><a href="https://eabsen.uui.ac.id/">Absensi Online</a></li>
													<li><a href="https://data.uui.ac.id/">Portal Data</a></li>
												</ul>
											</li> -->
											<li><a href="<?php echo base_url(); ?>id/dosen">Dosen</a></li>
											<li class="">
												<a href="<?php echo base_url(); ?>id">Tentang</a>
											</li>
											<!-- <li><a href="services.html">Pendaftaran</a></li> -->
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<footer class="site-footer bg-secondary mt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="row">
					<div class="col-6 col-md-4 col-lg-8 mb-5 mb-lg-0">
						<!-- <h3 class="footer-heading mb-4 text-light">About</h3> -->
						<img src="<?= base_url() ;?>assets/images/logo.png" alt="logouui" class="img-thumbnail mb-1" height="120" width="100">
						<p class="text-white-opacity-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat quos rem
							ullam, placeat
							amet sint
							vel impedit reprehenderit eius expedita nemo consequatur obcaecati aperiam, blanditiis quia iste in!
							Assumenda, odio?</p>
						<p><a href="#" class="btn btn-primary text-white px-4">Selengkapnya..</a></p>
					</div>
					<div class="col-6 col-md-4 col-lg-4 mb-5 mb-lg-0">
						<h3 class="footer-heading mb-4 text-light">Quick Menu</h3>
						<ul class="list-unstyled">
							<li><a class="text-white-opacity-5" href="https://sipenmaru.uui.ac.id">Sipenmaru</a></li>
							<li><a class="text-white-opacity-5" href="https://elearning.uui.ac.id">Elearning</a></li>
							<li><a class="text-white-opacity-5" href="https://simtakp.uui.ac.id">Simtakp</a></li>
							<li><a class="text-white-opacity-5" href="https://library.uui.ac.id">Perpustakaan</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="row mb-5">
					<div class="col-md-12">
						<h3 class="footer-heading mb-4 text-light">Contact Info</h3>
					</div>
					<div class="col-md-6">
						<p class="text-black">Jl. Alue Naga, Kec. Syiah Kuala
							Desa Tibang <br> Banda Aceh-Indonesia</p>
					</div>
					<div class="col-md-6 text-black">
						Tel. + (0651)-7555566 <br>
						Email. info[at].uui.ac.id

					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h3 class="footer-heading mb-4 text-light">Social Icons</h3>
					</div>
					<div class="col-md-12">
						<p>
							<a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
							<a href="#" class="p-2"><span class="icon-youtube-play"></span></a>
						</p>
					</div>
				</div>

			</div>
		</div>
		<hr>
		<div class="row pt-5 text-center">
			<div class="col-md-12">
				<p class="text-white-opacity-5">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy; <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
					<script>
						document.write(new Date().getFullYear());

					</script> All Rights Reserved | ICT Development Center Universitas Ubudiyah Indonesia
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</div>
</footer>
</div>

<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/aos.js"></script>

<script src="<?php echo base_url();?>assets/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/translator.js"></script>


<script>
	$(document).ready(function () {
		// grab the initial top offset of the navigation 
		var stickyNavTop = $('.nav').offset().top;

		// our function that decides weather the navigation bar should have "fixed" css position or not.
		var stickyNav = function () {
			var scrollTop = $(window).scrollTop(); // our current vertical position from the top

			// if we've scrolled more than the navigation, change its position to fixed to stick to top,
			// otherwise change it back to relative
			if (scrollTop > stickyNavTop) {
				$('.nav').addClass('sticky');
			} else {
				$('.nav').removeClass('sticky');
			}
		};

		stickyNav();
		// and run it again every time you scroll
		$(window).scroll(function () {
			stickyNav();
		});
	});

	// news section
	$(document).ready(function () {
		$(".linkfeat").hover(
			function () {
				$(".textfeat").show(500);
			},
			function () {
				$(".textfeat").hide(500);
			}
		);
	});

</script>

<!-- readmore -->
<script>
	(function ($) {
		$.fn.shorten = function (settings) {
			"use strict";

			var config = {
				showChars: 600,
				minHideChars: 10,
				ellipsesText: "...",
				moreText: "more",
				lessText: "<b>&#8592;Kembali</b>",
				onLess: function () {},
				onMore: function () {},
				errMsg: null,
				force: false
			};

			if (settings) {
				$.extend(config, settings);
			}

			if ($(this).data('jquery.shorten') && !config.force) {
				return false;
			}
			$(this).data('jquery.shorten', true);

			$(document).off("click", '.morelink');

			$(document).on({
				click: function () {

					var $this = $(this);
					if ($this.hasClass('less')) {
						$this.removeClass('less');
						$this.html(config.moreText);
						$this.parent().prev().animate({
							'height': '0' + '%'
						}, function () {
							$this.parent().prev().prev().show();
						}).hide('fast', function () {
							config.onLess();
						});

					} else {
						$this.addClass('less');
						$this.html(config.lessText);
						$this.parent().prev().animate({
							'height': '100' + '%'
						}, function () {
							$this.parent().prev().prev().hide();
						}).show('fast', function () {
							config.onMore();
						});
					}
					return false;
				}
			}, '.morelink');

			return this.each(function () {
				var $this = $(this);

				var content = $this.html();
				var contentlen = $this.text().length;
				if (contentlen > config.showChars + config.minHideChars) {
					var c = content.substr(0, config.showChars);
					if (c.indexOf('<') >= 0) // If there's HTML don't want to cut it
					{
						var inTag = false; // I'm in a tag?
						var bag = ''; // Put the characters to be shown here
						var countChars = 0; // Current bag size
						var openTags = []; // Stack for opened tags, so I can close them later
						var tagName = null;

						for (var i = 0, r = 0; r <= config.showChars; i++) {
							if (content[i] == '<' && !inTag) {
								inTag = true;

								// This could be "tag" or "/tag"
								tagName = content.substring(i + 1, content.indexOf('>', i));

								// If its a closing tag
								if (tagName[0] == '/') {


									if (tagName != '/' + openTags[0]) {
										config.errMsg = 'ERROR en HTML: the top of the stack should be the tag that closes';
									} else {
										openTags.shift(); // Pops the last tag from the open tag stack (the tag is closed in the retult HTML!)
									}

								} else {
									// There are some nasty tags that don't have a close tag like <br/>
									if (tagName.toLowerCase() != 'br') {
										openTags.unshift(tagName); // Add to start the name of the tag that opens
									}
								}
							}
							if (inTag && content[i] == '>') {
								inTag = false;
							}

							if (inTag) {
								bag += content.charAt(i);
							} // Add tag name chars to the result
							else {
								r++;
								if (countChars <= config.showChars) {
									bag += content.charAt(i); // Fix to ie 7 not allowing you to reference string characters using the []
									countChars++;
								} else // Now I have the characters needed
								{
									if (openTags.length > 0) // I have unclosed tags
									{
										//console.log('They were open tags');
										//console.log(openTags);
										for (j = 0; j < openTags.length; j++) {
											//console.log('Cierro tag ' + openTags[j]);
											bag += '</' + openTags[j] + '>'; // Close all tags that were opened

											// You could shift the tag from the stack to check if you end with an empty stack, that means you have closed all open tags
										}
										break;
									}
								}
							}
						}
						c = $('<div/>').html(bag + '<span class="ellip">' + config.ellipsesText + '</span>').html();
					} else {
						c += config.ellipsesText;
					}

					var html = '<div class="shortcontent">' + c +
						'</div><div class="allcontent">' + content +
						'</div><span><a href="javascript://nop/" class="morelink">' + config.moreText + '</a></span>';

					$this.html(html);
					$this.find(".allcontent").hide(); // Hide all text
					$('.shortcontent p:last', $this).css('margin-bottom', 0); //Remove bottom margin on last paragraph as it's likely shortened
				}
			});

		};

	})(jQuery);

	$(document).ready(function () {
		$('.list-group-item').shorten({
			moreText: '<b>Selengkapnya &#8594;</b>'
		});
	});

</script>

<script type="text/javascript">
function googleTranslateElementInit() {
	new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
}

</script>

<!-- ############################################################################################# -->
</body>

</html>

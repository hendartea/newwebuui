<div class="site-section block-14">
	<div class="container">
		<h1 class="display-5 mb-4">Tentang Universitas Ubudiyah Indonesia</h1>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<div class="list-group">
					<a href="<?= base_url() ; ?>id" class="list-group-item list-group-item-action active">
						<?= $judul_singkat; ?>
					</a>
					<a href="<?= base_url() ; ?>id/maknalogo" class="list-group-item list-group-item-action">Makna Logo</a>
					<a href="<?= base_url() ; ?>id/fasilitas" class="list-group-item list-group-item-action">Fasilitas</a>
					<a href="<?= base_url() ; ?>id/organisasi" class="list-group-item list-group-item-action">Struktur Organisasi</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">Renstra dan Proker</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">UUI Member</a>
				</div>
			</div>
			<div class="col-md-8">
				<h3><?= $judul_singkat; ?></h3>
				<div class="list-group">
					<div class="list-group-item">
						<p> Selamat datang di kampus Universitas UBudiyah Indonesia (UUI), universitas yang mendedikasikan diri untuk
							mencetak para ilmuan, peneliti, profesional dan pemimpin Aceh masa depan. Di kampus yang memiliki visi menjadi
							World Class University ini mahasiswa akan difasiltiasi dengan berbagai kemudahan berbasis teknologi informasi
							dan komunikasi (TIK) yang dikendalikan oleh Badan Perencana Sistem Informasi (BAPSI) UUI.
						</p>

						<p> Kemudahan dari fasilitas ini antara lain memungkin mahasiswa terkoneksi dengan sistem informasi akademik
							(SIKAD)
							perkuliahan, seperti Kartu Rencana Studi (KRS) online, informasi perkuliahan secara online dan menggunakan
							fasilitas wireless fidelity (wifi) di lingkungan kampus. UUI menawarkan sejumlah program studi favorit yang
							berada di bawah manajemen enam fakultas, yaitu Fakultas Ilmu Kesehatan, Fakultas Ekonomi, Fakultas Hukum,
							Fakultas Teknik dan Fakultas Keguruan, Ilmu Pendidikan (FKIP) dan Fakultas Ilmu Komputer. Program-program studi
							yang kami tawarkan selaras dengan trend dan perkembangan dunia kerja saat ini, sehingga lulusan UUI nantinya
							memiliki peluang kerja yang luas dan beragam sesuai dengan kompetensi yang dimiliki.
						</p>

						<p> Ada banyak keunggulan yang dimiliki UUI dengan memperkuat kompetensi lulusannya dengan serangkaian sertifikat
							kompetensi yang diberikan melalui pelatihan public speaking, kewirausahaan, Bahasa Inggris dan kompetensi khusus
							sesuai program studi masing-masing.
						</p>

						<p> Di samping itu, UUI dan program studinya juga terakreditasi pada Badan Akreditasi Nasional Perguruan Tinggi
							(BAN-PT), sehingga keberadaannya sah dan legal menurut ketentuan hukum dan undang-undang yang berlaku.
						</p>

						<p> Atas kiprahnya selama hampir satu dekade terakhir, UUI yang sebelumnya terdiri dari dua sekolah tinggi,
							STIKES
							U’Budiyah dan STMIK U’Budiyah, berhasil meraih sejumlah penghargaan seperti, (1) Certified Education and
							Educator
							Indonesia (CEEI) Award dari Pusat Rekor Indonesia; (2) Top Ten Most Trusted School dari Indonesia Achiement
							Centre (dua kategori) dan (3) Platinum Indonesia 2013-2014 Award dari Pusat Rekor Indonesia; (4) Indonesia Top
							Figure Inovative Award 2014 As The Best Education Figures Of The Year 2014; (5) Indonesia Best 50 Trusted
							Companies Award 2014 As the Best Reputable Hospital in Service Excellent Of The Year 2014. Penghargaan tersebut
							adalah pencapaia yang harus mampu member dorongan lebih lanjut bagi seluruh sivitas akademika UUI untuk meraih
							yang terbaik pada saat ini dan di masa depan.
						</p>

						<p> Untuk mewujudkan Visi 2025 : Menjadi World Class University, UUI telah merintis sejumlah kerjasama dalam dan
							luar
							negeri. Kerjasama dalam negeri antara lain dengan Unviersitas Gunadarma Jakarta, Universitas Islam Sultan Agung
							(Unissula) Semarang, Universitas Syiah Kuala dan Poltekkes Kemenkes Yogyakarta.
						</p>

						<p> Beberapa kerja sama luar negeri yang sudah dirintis antara lain dengan Universiti Malaysia Perlis (Unimap),
							Budapest Business School (BBS), Semmelweis University, PEC’s University (Hungaria) University of Zagreb,
							University of Dubrovnik, University of Zadar, Josijuraj Strossmayer University of Osijek, Juraj Dobrilla
							University of Pula (Kroasia). Di Spanyol, UBudiyah juga membangun kerjasama dengan CEDES dan SPAINDO.
						</p>


						<p> Kerja sama luar negeri tersebut antara lain meliputi bidang penelitian, program gelar ganda (double degree),
							publikasi jurnal internasional dan pendidikan S-2 dan S-3 dan pelatihan singkat (short course) bagi dosen dan
							mahasiswa. Pada November 2013 lalu, UUI juga terdaftar sebagai anggota Asian University Presiden Forum (AUPF)
							ke-13 di Langkawi Malaysia yang diikuti puluhan perguruan tinggi dari 22 negara di Asia. Implementasi kerja sama
							ini mulai direalisasikan pada tahun ini dengan keikutsertaan mahasiswa UBudiyah pada program Asia Summer Program
							(ASP). Sementara itu untuk penunjang sarana akademik, UUI dilengkapi dengan fasilitas laboratorium, gedung
							kuliah
							dan perpustakaan yang modern dengan staf pengajar lulusan master (S2) dan doctor (S3). Sedangkan untuk kegiatan
							kemahasiswaan, ada organisasi BEM serta sejumlah unit kegiatan mahasiswa (UKM) diantaranya U’Budiyah FC, UKM
							Tinju, Tarung Drajat, English Club, UKM Pengguna Open Source (POS), UKM MPK, UKM Voli, UKM Ekspresi, UKM LPM dan
							sanggar tari UBudiyah Heritage.
						</p>

						<p> Pada tahun 2014 ini, UUI menghadirkan satu inovasi baru bagi mahasiswa dengan diluncurkannya handphone UUI
							Android IMO S7. Inovasi ini memungkin mahasiswa UUI mengakses informasi akademik secara cepat hanya dari
							genggaman. Inovasi ini merupakan hasil kerjasama UUI dengan Universitas Gunadarma dan IMO. Untuk mahasiswa baru
							UUI akan mendapatkan fasilitas handphone ini akan dibagikan secara gratis.
						</p>

						<p> Bagi Anda yang ingin meraih karir yang tepat dan ingin mewujudkan cita-cita menjadi ilmuan, peneliti,
							professional dan pemimpin Aceh masa depan, memilih UUI adalah satu keputusan tepat dan hebat.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

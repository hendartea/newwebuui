<div class="site-section block-14">
	<div class="container">
		<h1 class="display-5 mb-4">Tentang Universitas Ubudiyah Indonesia</h1>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<div class="list-group">
					<a href="<?= base_url() ; ?>id" class="list-group-item list-group-item-action">
               Sejarah Singkat
					</a>
					<a href="<?= base_url() ; ?>id/maknalogo" class="list-group-item list-group-item-action">Makna Logo</a>
					<a href="<?= base_url() ; ?>id/fasilitas" class="list-group-item list-group-item-action">Fasilitas</a>
					<a href="<?= base_url() ; ?>id/organisasi" class="list-group-item list-group-item-action active">Struktur Organisasi</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">Renstra dan Proker</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">UUI Member</a>
				</div>
			</div>
			<div class="col-md-8">
				<h3><?= $organisasi; ?></h3>
				<div class="list-group img-fluid ">
					<img src="<?= base_url();?>assets/images/ldo-STRUKTUR-UUI.jpg" alt="strukturuui">
				</div>
			</div>
		</div>
	</div>
</div>

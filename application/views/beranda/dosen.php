<div class="site-section bg-light">

	<div class="container">

		<div class="heading-with-border text-center mb-5">
			<h2 class="heading text-uppercase">
				<?= $titledosen ; ?>
			</h2>
		</div>

		<div class="row">

			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_4.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>M. Bayu Wibawa</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_3.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>Zalfie Ardian</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>

			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_4.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>Nuzul Rahmi</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_3.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>Ulfa Farah Lisa</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_2.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>Jumaidi Syahputra</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>

			<div class="col-lg-4 mb-4">
				<div class="block-trainer">
					<img src="<?= base_url();?>assets/images/person_1.jpg" alt="Image" class="img-fluid">
					<div class="block-trainer-overlay">
						<h2>Anhar Nasution</h2>
						<p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quas iste corporis
							asperiores placeat earum.</p>
						<p>
							<a href="#" class="p-2"><span class="icon-facebook"></span></a>
							<a href="#" class="p-2"><span class="icon-twitter"></span></a>
							<a href="#" class="p-2"><span class="icon-instagram"></span></a>
						</p>
						<a href="#" class="btn btn-danger pill">My Direktori</a>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

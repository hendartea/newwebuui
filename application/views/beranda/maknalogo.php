<div class="site-section block-14">
	<div class="container">
		<h1 class="display-5 mb-4">Tentang Universitas Ubudiyah Indonesia</h1>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<div class="list-group">
					<a href="<?= base_url() ; ?>id" class="list-group-item list-group-item-action">
						Sejarah Singkat
					</a>
					<a href="<?= base_url() ; ?>id/maknalogo" class="list-group-item list-group-item-action active">Makna Logo</a>
					<a href="<?= base_url() ; ?>id/fasilitas" class="list-group-item list-group-item-action">Fasilitas</a>
					<a href="<?= base_url() ; ?>id/organisasi" class="list-group-item list-group-item-action">Struktur Organisasi</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">Renstra dan Proker</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">UUI Member</a>
				</div>
			</div>
			<div class="col-md-8">
				<h3><?= $judul_logo; ?></h3>
				<div class="list-group">
					<div align="center" class="mt-2 mb-3">
						<img src="<?= base_url(); ?>assets/images/logo.png" alt="logouui2" class="img-thumbnail rounded" height="200"
						 width="200">
					</div>
					<div class="list-group-item">
						<h5> 1. Segi Empat Kotak </h5>
						<p> Melambangkan universitas U’budiyah Indonesia selalu menjunjung tinggi keutuhan dan kesatuan dalam satu wadah
							untuk kepentingan bersama.</p>

						<h5>2. Pintu Aceh</h5>
						<p> Melambangkan gerbang ilmu pengetahuan berwawasan nasional juga berisikan muatan lokal propinsi aceh tempat
							berdirinya Universitas U’budiyah Indonesia.</p>

						<h5> 3.Toga</h5>
						<p> Melambangkan kesuksesan dan kejayaan lulusan yang siap terjun ke dunia kerja.</p>

						<h5> 4. Buku </h5>
						<p> Melambangkan sumber ilmu pengetahuan yang selalu di utamakan dalam upaya meningkatkan sumber daya manusia di
							lingkungan Universitas U’budiyah Indonesia.</p>

						<h5>5. Gambar Tunas</h5>
						<p> Universitas U’budiyah Indonesia sebagai cikal bakal wadah pendidikan yang terus berkembang.</p>

						<h5>6. Tulisan Kami Memimpin</h5>
						<p> Melambangkan Universitas U’budiyah Indonesia selalu terdepan dalam kualitas.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

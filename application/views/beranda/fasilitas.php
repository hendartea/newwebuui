<div class="site-section block-14">
	<div class="container">
		<h1 class="display-5 mb-4">Tentang Universitas Ubudiyah Indonesia</h1>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<div class="list-group">
					<a href="<?= base_url() ; ?>id" class="list-group-item list-group-item-action">
               Sejarah Singkat
					</a>
					<a href="<?= base_url() ; ?>id/maknalogo" class="list-group-item list-group-item-action">Makna Logo</a>
					<a href="<?= base_url() ; ?>id/fasilitas" class="list-group-item list-group-item-action active">Fasilitas</a>
					<a href="<?= base_url() ; ?>id/organisasi" class="list-group-item list-group-item-action">Struktur Organisasi</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">Renstra dan Proker</a>
					<a href="<?= base_url() ; ?>id/#" class="list-group-item list-group-item-action">UUI Member</a>
				</div>
			</div>
			<div class="col-md-8">
				<h3><?= $fasilitas; ?></h3>
				<div class="list-group">
					<div class="list-group-item">
						<h5>Gedung Kampus</h5>
						<p> Gedung kampus Universitas U’Budiyah Indonesia berada pada lokasi yang strategis di Jl. Alue Naga Desa Tibang, Krueng Cut, Banda Aceh. Gedung kampus mempunyai fasilitas terlengkap terdiri dari gedung kuliah berlantai 2 (dua), 3 (tiga) ruang laboratorium komputer, laboratorium kompetensi untuk ujian akhir mahasiswa, Laboratorium bahasa terlengkap di Nanggroe Aceh Darussalam, dan perpustakaan yang lengkap dengan buku-buku yang up to date dan mutakhir.
						</p>

						<h5>Fasilitas Pengajaran</h5>
						<p> Media pengajaran yang tersedia di Universitas UBudiyah Indonesia berupa audio visual, LCD proyektor di setiap kelasnya. Kurikulum yang digunakan, dirancang untuk memberikan kompetensi kepada mahasiswa dan match dengan dunia kerja dan telah disesuaikan dengan perkembangan dunia kesehatan. Setiap mata kuliah diasuh oleh dosen pendidikan dan dosen praktisi dari perguruan tinggi/perusahaan/instansi yang profesional di bidangnya dengan kualifikasi S1, S2 dan S3.
						</p>
						
                  <h5>Pembelajaran Berbasis Elearning</h5>
						<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita magni quae cumque reprehenderit alias vitae iure, totam eaque neque eligendi nihil unde sit similique amet magnam ex quis quod. Ut.
						</p>
                  
                  <h5>Fasilitas Laboratorium</h5>
						<p> Fasilitas laboratorium yang ada di UNIVERSITAS UBUDIYAH INDONESIA antara lain: Laboratorium Kebidanan, Laboratorium Kesehatan Masyarakat dan 3 (tiga) ruang laboratorium komputer dengan sistem LAN yang memungkinkan untuk mengakses internet, laboratorium kompetensi yang dapat digunakan oleh mahasiswa yang sedang menyelesaikan Tugas Akhir, dan laboratorium bahasa yang dilengkapi dengan fasilitas yang sangat memadai untuk meningkatkan kemampuan mahasiswa dalam berbahasa inggris agar dapat lulus pada seleksi penerimaan pegawai negeri dan swasta di dunia kerja.
						</p>
                  
                  <h5>Perpustakaan</h5>
						<p> Perpustakaan yang tersedia di UNIVERSITAS U’BUDIYAH INDONESIA dilengkapi dengan buku-buku yang relevan dengan bidang informatika dan komputer untuk program studi Teknik Informatika, Sistem Informasi, Manajemen Informatika dan Komputerisasi Akuntansi. Jumlah item buku yang tersedia lebih dari 500 judul buku, yang dapat dipergunakan oleh mahasiswa untuk menunjang kegiatan perkuliahan.</p>
                  
                  <p>
                  Perpustakaan UNIVERSITAS U’BUDIYAH INDONESIA dilengkapi dengan buku literatur baik yang berbahasa indonesia mapun yang berbahasa inggris, jurnal ilmiah, majalah serta buku ilmu pengetahuan lainnya. Semua fasilitas yang terdapat pada perpustakaan UNIVERSITAS U’BUDIYAH INDONESIA dapat dimanfaatkan oleh mahasiswa, dosen, karyawan dan alumni UNIVERSITAS U’Budiyah Indonesia. Sebagai sumber informasi bagi pelaksanaan proses belajar dan mengajar, penelitian dan pengabdian pada masyarakat
						</p>

                  <h5>Lapangan Olah Raga</h5>
						<p> Selain fasilitas yang berkaitan dengan bidang akademis, Universitas U’Budiyah Indonesia juga mempunyai sarana yang dapat digunakan untuk kegiatan ekstrakurikuler yaitu lapangan olah raga dan Lapangan Volley. .</p>
                  
                  <h5>Mesjid/ Musholla</h5>
						<p> UNIVERSITAS U’BUDIYAH INDONESIA memiliki 1 buah mushalla. Mushalla ini digunakan sebagai sarana ibadah  dan mahasiswa dilingkungan UNIVERSITAS U’BUDIYAH INDONESIA serta masyarakat sekitar. Selain itu mushalla ini juga digunakan sebagai tempat kegiatan atau pertemuan kerohanian mahasiswa dan dosen.</p>
                  
                  <h5>UBudiyah Plenary Hall</h5>
						<p> Auditorium yang dimiliki Universitas U’Budiyah Indonesia biasa digunakan sebagai tempat pelatihan, seminar/workshop dan digunakan sebagai tempat yudisium dan wisuda.</p>
                  
                  <h5>Lahan Praktek</h5>
						<p> UNIVERSITAS U’BUDIYAH INDONESIA mempunyai sinergitas dengan banyak instansi kesehatan baik negri maupun swasta baik regional maupun nasional, maka dengan ini para mahasiswa fakultas kesehatan dapat lebih memahami dan menerapkan keilmuan Praktikum sehingga mampu menjadi kader-kader yang berkualitas dan siap dalam menghadapi segala dinamika dan perkembangan di dunia kesehatan.</p>



						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

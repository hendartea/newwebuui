<div class="slide-one-item home-slider owl-carousel">
	<div class="site-blocks-cover" style="background-image: url(assets/images/f.jpg);" data-aos="fade"
	 data-stellar-background-ratio="0.5">
		<div class="row justify-content-center teksslide" data-aos="fade">
			<h1 class="mototext">Creative, <strong>Inovative</strong>, Entrepreneurship &amp; <strong> Leadership</strong></h1>
		</div>
	</div>

	<div class="site-blocks-cover" style="background-image: url(assets/images/a.jpg);" data-aos="fade"
	 data-stellar-background-ratio="0.5">
		<div class="row justify-content-center teksslide" data-aos="fade">
			<h1 class="mototext">Membangun Bersama untuk <strong>Pendidikan</strong></h1>
		</div>
	</div>

	<div class="site-blocks-cover" style="background-image: url(assets/images/e.jpg);" data-aos="fade"
	 data-stellar-background-ratio="0.5">
		<div class="row justify-content-center teksslide" data-aos="fade">
			<h1 class="mototext">Membangun <strong>Kreatifitas</strong> & <strong>Innovasi</strong></h1>
		</div>
	</div>
</div>

<div class="site-section block-14">
	<div class="container">

		<div class="heading-with-border text-center">
			<h2 class="heading text-uppercase">Sambutan</h2>
		</div>

		<div class="nonloop-block-14 owl-carousel">
			<div class="d-flex block-testimony ml-2">
				<div class="person mr-3">
					<img src="assets/images/dedi.jpeg" alt="Image" class="img-fluid rounded-circle">
				</div>
				<div>
					<h2 class="h5">Ketua Yayasan Ubudiyah, CEO</h2>
					<blockquote class="text-justify">&ldquo;Puji syukur kami panjatkan kepada Allah SWT atas rahmat dan hidayah-Nya
						sehingga portal
						website Universitas UBudiyah Indonesiaini telah resmi di publikasi. Situs web ini di bangun sebagai salah satu
						media information center kampus dalam hal transformasi informasi secara cepat dan tepat kepada seluruh mahasiswa
						dan masyarakat. Semoga situs web ini dapat menjadi sarana penunjang untuk pengembangan Universitas UBudiyah
						Indonesia Menuju Kampus World Class Cyber University.&rdquo; <strong>Dedi Zefrizal, S.T</strong></blockquote>
				</div>
			</div>
			<div class="d-flex block-testimony ml-2">
				<div class="person mr-3">
					<img src="assets/images/rektor.jpg" alt="Image" class="img-fluid rounded-circle">
				</div>
				<div>
					<h2 class="h5">Rektor Universitas Ubudiyah Indonesia</h2>
					<blockquote class="text-justify">&ldquo;Selamat datang mahasiswa baru di kampus Universitas Ubudiyah Indonesia
						(UUI). UUI adalah
						perguruan tinggi yang mengemban misi mulia dalam mencetak para ilmuan dan pemimpin Aceh masa depan. Di kampus
						yang memiliki visi menjadi World Class Cyber University ini mahasiswa akan difasiltiasi dengan berbagai kemudahan
						berbasis teknologi informasi dan komunikasi (TIK) dan multimedia.&rdquo; <strong>Marniati, M.Kes</strong>
					</blockquote>
				</div>
			</div>
			<div class="d-flex block-testimony ml-2">
				<div class="person mr-3">
					<img src="assets/images/datok.jpg" alt="Image" class="img-fluid rounded-circle">
				</div>
				<div>
					<h2 class="h5">Penasehat UUI</h2>
					<blockquote class="text-justify">&ldquo;Datuk Prof Dr Kamarudin Hussin (vice-chancellor UniMAP) merupakan vice
						chancellor pertama
						UniMAP dan masih menduduki jabatan sebagai vice choncellor hingga saat ini dan telah berhasil membawa UniMAP di
						rangking Top Universities dunia. Award tersebut diserahkan oleh Raja Muda of Perlis, Tuanku Syed Faizuddin Putra
						Jamalullail pada acara the Special Convocation Ceremony Universiti Malaysia Perlis. Selamat dan Sukses Kepada
						Penasehat UUI Datuk Prof Dr Kamarudin Hussin (vice-chancellor UniMAP)&rdquo; <strong>Datuk Prof Dr Kamarudin
							Hussin</strong> </blockquote>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- NEWS -->
<div class="berita bg-light pb-2">
	<div class="container">
		<div class="heading-with-border display-5 text-left">
			<h1 class="heading text-uppercase ">Berita Utama <strong>UUI</strong> </h1>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6  py-0 pl-3 pr-1 featcard">
				<div id="featured" class="carousel slide carousel-fade" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="card bg-dark text-white">
								<img class="card-img img-fluid" src=" <?= base_url(); ?>assets/images/hall2.jpg" alt="news" height="300">
								<div class="card-img-overlay d-flex linkfeat">
									<a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
										<span class="badge">News</span>
										<h4 class="card-title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, ipsam!</h4>
										<p class="textfeat" style="display: none;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic,
											iste vel esse distinctio doloremque magni magnam! Tenetur nesciunt reiciendis sit modi ipsa placeat
											quibusdam corrupti ab, laboriosam, ut repellendus mollitia, saepe est minus libero facere dolor aliquam
											perspiciatis delectus aliquid. Hic dolorem voluptas rem natus veritatis sed veniam deleniti repellat.</p>
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="card bg-dark text-white">
								<img class="card-img img-fluid" src=" <?= base_url(); ?>assets/images/hall2.jpg" alt="">
								<div class="card-img-overlay d-flex linkfeat">
									<a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
										<span class="badge">News</span>
										<h4 class="card-title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, ipsam!</h4>
										<p class="textfeat" style="display: none;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic,
											iste vel esse distinctio doloremque magni magnam! Tenetur nesciunt reiciendis sit modi ipsa placeat
											quibusdam corrupti ab, laboriosam, ut repellendus mollitia, saepe est minus libero facere dolor aliquam
											perspiciatis delectus aliquid. Hic dolorem voluptas rem natus veritatis sed veniam deleniti repellat.</p>
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="card bg-dark text-white">
								<img class="card-img img-fluid" src=" <?= base_url(); ?>assets/images/hall2.jpg" alt="">
								<div class="card-img-overlay d-flex linkfeat">
									<a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
										<span class="badge">News</span>
										<h4 class="card-title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, ipsam!</h4>
										<p class="textfeat" style="display: none;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic,
											iste vel esse distinctio doloremque magni magnam! Tenetur nesciunt reiciendis sit modi ipsa placeat
											quibusdam corrupti ab, laboriosam, ut repellendus mollitia, saepe est minus libero facere dolor aliquam
											perspiciatis delectus aliquid. Hic dolorem voluptas rem natus veritatis sed veniam deleniti repellat.</p>
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="card bg-dark text-white">
								<img class="card-img img-fluid" src=" <?= base_url(); ?>assets/images/hall2.jpg" alt="">
								<div class="card-img-overlay d-flex linkfeat">
									<a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
										<span class="badge">News</span>
										<h4 class="card-title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, ipsam!</h4>
										<p class="textfeat" style="display: none;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic,
											iste vel esse distinctio doloremque magni magnam! Tenetur nesciunt reiciendis sit modi ipsa placeat
											quibusdam corrupti ab, laboriosam, ut repellendus mollitia, saepe est minus libero facere dolor aliquam
											perspiciatis delectus aliquid. Hic dolorem voluptas rem natus veritatis sed veniam deleniti repellat.</p>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-6 py-0 px-1 d-none d-lg-block">

				<div class="row">
					<div class="col-6 pb-2 mg-1">
						<div class="card bg-dark text-white">
							<img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/bi-atur-standarisasi-qr-code-1529952479.jpg"
							 alt="">
							<div class="card-img-overlay d-flex">
								<a href="http://makro.id/bi-atur-standarisasi-qr-code" class="align-self-end">
									<span class="badge">Pengeumuman</span>
									<h6 class="card-title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, aliquam.</h6>
								</a>
							</div>
						</div>
					</div>
					<div class="col-6 pb-2 mg-2">
						<div class="card bg-dark text-white">
							<img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/ptsp-bp-batam-masuk-10-terbaik-di-indonesia-1531400445.jpeg"
							 alt="">
							<div class="card-img-overlay d-flex">
								<a href="http://makro.id/ptsp-bp-batam-masuk-10-terbaik-di-indonesia" class="align-self-end">
									<span class="badge">Fakultas</span>
									<h6 class="card-title">PTSP BP Batam Masuk 10 Terbaik di Indonesia</h6>
								</a>
							</div>
						</div>
					</div>
					<div class="col-6 pb-2 mg-3">
						<div class="card bg-dark text-white">
							<img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/review-gsp-amerika-ingin-perdagangan-yang-adil-dan-saling-menguntungkan-1531307731.jpg"
							 alt="">
							<div class="card-img-overlay d-flex">
								<a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
									<span class="badge">Prodi</span>
									<h6 class="card-title">Review GSP: Amerika Ingin Perdagangan Saling Menguntungkan</h6>
								</a>
							</div>
						</div>
					</div>
					<div class="col-6 pb-2 mg-4">
						<div class="card bg-dark text-white">
							<img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/dpr-setujui-penambahan-anggaran-bp-batam-rp565-miliar-1531258457.jpeg"
							 alt="">
							<div class="card-img-overlay d-flex">
								<a href="http://makro.id/dpr-setujui-penambahan-anggaran-bp-batam-rp565-miliar" class="align-self-end">
									<span class="badge">Kegiatan</span>
									<h6 class="card-title">DPR Setujui Penambahan Anggaran BP Batam Rp565 Miliar</h6>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-4 heading-with-border text-center display-1">
				<h5 class="heading text-uppercase">Berita UUI</h5>
			</div>
			<div class="col-md-4 heading-with-border text-center">
				<h5 class="heading text-uppercase">Pengumuman UUI</h5>
			</div>
			<div class="col-md-4 heading-with-border text-center">
				<h5 class="heading text-uppercase">Event UUI</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small>3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small>3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small>3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">List group item heading</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small class="text-muted">Donec id elit non mi porta.</small>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="#" class="btn btn-primary text-white px-4 mb-3 mt-3 pill">Read More</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="btn btn-primary text-white px-4 mb-3 mt-3 pill">Read More</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="btn btn-primary text-white px-4 mb-3 mt-3 pill">Read More</a>
			</div>
		</div>
	</div>
</div>

<!-- End News -->

<!-- Agenda -->

<div class="block-schedule overlay site-section" style="background-image: url('assets/images/e.jpg');">
	<div class="container">

		<h2 class="text-white display-4 mb-5">Agenda</h2>

		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-sunday" role="tabpanel" aria-labelledby="pills-sunday-tab">
				<div class="row-wrap">
					<div class="row bg-white p-4 align-items-center">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3 class="h5">I+Aceh</h3>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-clock-o mr-3"></span>8:00am &mdash; 10:00am</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-person mr-3"></span> David Holmes</div>
						<div class="col-sm-3 col-md-3 col-lg-3 text-md-right"><a href="#" class="btn btn-primary pill px-4 mt-3 mt-md-0">Join
								Now</a></div>
					</div>
				</div>
				<div class="row-wrap">
					<div class="row bg-white p-4 align-items-center">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3 class="h5">Drugstect</h3>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-clock-o mr-3"></span>8:00am &mdash; 10:00am</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-person mr-3"></span> Bruce Mars</div>
						<div class="col-sm-3 col-md-3 col-lg-3 text-md-right"><a href="#" class="btn btn-primary pill px-4 mt-3 mt-md-0">Join
								Now</a></div>
					</div>
				</div>
				<div class="row-wrap">
					<div class="row bg-white p-4 align-items-center">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3 class="h5">ICOSTI</h3>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-clock-o mr-3"></span>8:00am &mdash; 10:00am</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-person mr-3"></span> Josh White</div>
						<div class="col-sm-3 col-md-3 col-lg-3 text-md-right"><a href="#" class="btn btn-primary pill px-4 mt-3 mt-md-0">Join
								Now</a></div>
					</div>
				</div>
				<div class="row-wrap">
					<div class="row bg-white p-4 align-items-center">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3 class="h5">IJCIMBI</h3>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-clock-o mr-3"></span>8:00am &mdash; 10:00am</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-person mr-3"></span> David Holmes</div>
						<div class="col-sm-3 col-md-3 col-lg-3 text-md-right"><a href="#" class="btn btn-primary pill px-4 mt-3 mt-md-0">Join
								Now</a></div>
					</div>
				</div>
				<div class="row-wrap">
					<div class="row bg-white p-4 align-items-center">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3 class="h5">SEMINAR</h3>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-clock-o mr-3"></span>8:00am &mdash; 10:00am</div>
						<div class="col-sm-3 col-md-3 col-lg-3"><span class="icon-person mr-3"></span> Bruce Mars</div>
						<div class="col-sm-3 col-md-3 col-lg-3 text-md-right"><a href="#" class="btn btn-primary pill px-4 mt-3 mt-md-0">Join
								Now</a></div>
					</div>
				</div>

			</div>

		</div>


	</div>

</div>
</div>
<!-- end agenda -->
<div class="site-section block-14 bg-warning">
	<div class="container">
		<div class="heading-with-border text-left">
			<h1 class="heading text-uppercase">UUI MEDIA</h1>
		</div>
		<div class="row">
			<div class="col-md-8 ">
				<iframe width="750" height="320" src="https://www.youtube.com/embed/a6Su4ARhgxM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				 allowfullscreen></iframe>
			</div>
			<div class="col-md-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Youtube Channel</h5>
						<p class="card-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illo aliquam illum consectetur
							iusto, porro voluptatibus voluptatum ea quidem consequuntur quod commodi neque velit quo necessitatibus
							cupiditate veritatis deleniti asperiores. Lorem ipsum dolor sit amet.</p>
						<a href="#" class="btn btn-primary">Go somewhere</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <img src="<?= base_url() ;?>assets/images/logo_ukm.jpg" alt="logoukm" style="width:100%" class="pt-4"> -->
</div>
